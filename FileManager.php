<?php namespace Import1C;

use Import1C\FailureException;
use ZipArchive;

class FileManager{

	protected $upload_path;

	function __construct(){
		$this->upload_path = config('upload_path');
	}

	public function cleanUploadPath(){
		array_map('unlink', glob($this->upload_path.'*'));
	}

	public function saveFile($filename){
		$fl = fopen($this->upload_path.basename($filename), 'a');
		$status = fwrite($fl, file_get_contents('php://input'));
		fclose($fl);
		if($status === false){
			throw new FailureException("Could not save file '".$_GET['filename']."' to upload path");
		}
	}

	public function extractZipIfExists(){
		$zip_path = glob($this->upload_path.'*.zip')[0];
		if(!$zip_path){
			return;
		}
		$zip = new ZipArchive();
		$errorCode = $zip->open($zip_path);
		if($errorCode !== true){
			throw new FailureException("Could not open zip file '".$zip_path."'. error code: ".$errorCode);
		}
		$zip->extractTo($this->upload_path);
        $zip->close();
		unlink($zip_path);
	}
}