<?php namespace Import1C;

class AuthGuard{
	public function check(){
		return $this->attempt($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']);
	}

	public function attempt($user, $password){
		return $user == config('user') && $password == config('password');
	}
}