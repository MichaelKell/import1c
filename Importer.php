<?php namespace Import1C;

use Import1C\DataProcessor;
use App\Abante\Modules\Logger;

class Importer{
	const STEP_THRESHOLD_SEC = 5;
    protected $dataProcessor;

	public function __construct(){
        $this->dataProcessor = new DataProcessor();
    }

	public function catalogue(){
        $xml = simplexml_load_file(config('filename_import'));
		if(empty($_SESSION['last_product'])){
            $this->dataProcessor->storeGroups($xml->Классификатор->Группы);
		}
        $products = $xml->Каталог->Товары->Товар;
		$total = count($products);
        $start = $_SESSION['last_product'];
        Logger::write('products import: '.$start.'/'.$total, Logger::DEBUG);
		for($i = $start; $i < $total; $i++){
            if($this->isTimeOver()){
                $_SESSION['last_product'] = $i;
                return "progress\nВыгружено товаров: $i / $total\n";
            }
			$this->dataProcessor->storeProduct($products[$i]);
		}
		$_SESSION['last_product'] = 0;
		return "success";
	}

    public function offers(){
		$xml = simplexml_load_file(config('filename_offers'));
        $offers = $xml->ПакетПредложений->Предложения->Предложение;
		$total = count($offers);
        $start = $_SESSION['last_offer'];
        Logger::write('offers import: '.$start.'/'.$total, Logger::DEBUG);
		for($i = $start; $i < $total; $i++){
			if($this->isTimeOver()){
				$_SESSION['last_offer'] = $i;
				return "progress\nВыгружено предложений: $i / $total\n";
			}
            $this->dataProcessor->storeOffer($offers[$i]);
		}
		$_SESSION['last_offer'] = 0;
		return "success";
    }

	public function isTimeOver(){
        return (microtime(true) - $GLOBALS['start_time'] + self::STEP_THRESHOLD_SEC) >= config('max_exec_time');
	}

	public function resetProgress(){
		$_SESSION['last_product'] = $_SESSION['last_offer'] = 0;
	}

	public function catalogueAfter(){
		/* ... */
	}

	public function offersAfter(){
        $this->dataProcessor->deleteEmptyCategories();
	}
}