<?php

function config($name){
	static $config;
	$config = require('config.php');
	return $config[$name];
}

function dateStr(){
    return date('Y-m-d H:i:s');
}