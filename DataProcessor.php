<?php namespace Import1C;

use simpleXMLElement;
use App\Abante\Modules\Db;
use App\Abante\Modules\Logger;

class DataProcessor{

    public function storeProduct($product){
        $external_id = (string)$product->Ид;
        if($product->Статус == 'Удален'){
            $this->deleteProduct($external_id);
            return;
        }
        $stmt = db::prepare("INSERT INTO CatItems SET
            created=1,
            active=1,
            1c_new=1,
            1c_last_synced=:1c_last_synced,
            name=:name,
            manufacturer=:manufacturer,
            body=:body,
            category_1c=:category_1c,
            external_id=:external_id
            ON DUPLICATE KEY UPDATE SET
            1c_last_synced=:1c_last_synced,
            name=:name,
            manufacturer=:manufacturer,
            body=:body
        ");
        $stmt->bindValue(':1c_last_synced', dateStr());
        $stmt->bindValue(':name', (string)$product->Наименование);
        $stmt->bindValue(':external_id', $external_id);
        $stmt->bindValue(':manufacturer', (string)$product->Изготовитель->Наименование);
        $stmt->bindValue(':body', (string)$product->Описание);
        $stmt->bindValue(':category_1c', (string)$product->Группы->Ид);
        $stmt->execute();
        $this->storeProductImages(db::lastInsertId(), $product->Картинка);
    }

    public function storeOffer($offer){
        $stmt = db::prepare("UPDATE CatItems SET price=:price, in_stock=:in_stock WHERE external_id=:external_id");
        $stmt->bindValue(':external_id', (string)$offer->Ид);
        $stmt->bindValue(':price', (float)$offer->Цены->Цена[0]->ЦенаЗаЕдиницу);
        $stmt->bindValue(':in_stock', (int)$offer->Количество);
        $stmt->execute();
    }

    public function storeGroups($groups, $parent_1c = 0, $level=0){
        $total = count($groups->Группа);
        for($i = 0; $i < $total; $i++){
            $external_id = (string)$groups->Группа[$i]->Ид;
            $stmt = db::prepare("INSERT INTO CatCategories1C SET
                1c_last_synced=:1c_last_synced,
                parent=:parent,
                name=:name,
                external_id=:external_id
                ON DUPLICATE KEY UPDATE SET
                1c_last_synced=:1c_last_synced,
                parent=:parent,
                name=:name
            ");
            $stmt->bindValue(':1c_last_synched', dateStr());
            $stmt->bindValue(':parent', $parent_1c);
            $stmt->bindValue(':name', (string)$groups->Группа[$i]->Наименование);
            $stmt->bindValue(':external_id', $external_id);
            $stmt->execute();
            if($groups->Группа[$i]->Группы){
                $this->storeGroups($groups->Группа[$i]->Группы, $external_id, $level+1);
            }
        }
    }

    protected function storeProductImages($catitem_id, $images){
        foreach ($images as $img) {
            $basename = basename($img);
            $url = '/images/data/1c/'.$basename;
            $source = config('upload_path').$basename;
            if(!rename($source, $url)){
                Logger::write('Could not move CatItem file '.$source, Logger::NOTICE);
                continue;
            }
            $stmt = db::prepare("INSERT IGNORE INTO CatItems_files SET catitem_id=:catitem_id, url=:url");
            $stmt->bindValue(':catitem_id', $catitem_id);
            $stmt->bindValue(':url', $url);
            $stmt->execute();
        }
    }

    protected function deleteProduct($external_id){
        $files = db::column("SELECT url FROM CatItems_files LEFT JOIN CatItems ON CatItems_files.catitem_id=CatItems.id WHERE CatItems.external_id='".$external_id."'");
        array_map('unlink', $files);
        $this->query("DELETE FROM CatItems where external_id='".$external_id."'");
    }

    public function deleteEmptyCategories(){
        $query = "
            SELECT CatCategories1C.external_id as id
            FROM CatCategories1C
            LEFT JOIN CatCategories1C  AS cat2 ON cat2.Parent=CatCategories1C.external_id
            LEFT JOIN CatItems ON CatCategories1C.external_id=CatItems.Category1C
            WHERE CatItems.id IS NULL AND cat2.id IS NULL
            GROUP BY CatCategories1C.external_id
	    ";
        $cnt = db::field("SELECT COUNT(id) as cnt FROM ($query) as tmp");
        $count = 0;
        while($cnt > 0){
            db::query("DELETE FROM CatCategories1C WHERE id IN(SELECT id FROM ( $query ) tmp )");
            $cnt = db::field("SELECT COUNT(id) as cnt FROM ($query) as tmp");
            $count += $cnt;
        }
        Logger::write('Deleted empty categories count: '.$count, Logger::DEBUG);
    }
}