<?php namespace Import1C;

use Import1C\AuthGuard;
use Import1C\LockerGuard;
use Import1C\FileManager;
use Import1C\Importer;
use Import1C\FailureException;
use App\Abante\Modules\Logger;
use Exception;

class FrontController{

	protected $auth;
	protected $locker;
	protected $fileManager;
    protected $importer;
    protected $after;
	const INVOKABLE_METHODS = ['checkauth', 'init', 'file', 'import'];

	public function __construct(){
		$this->auth = new AuthGuard();
		$this->locker = new LockerGuard(config('locker_path'), 300);
		$this->fileManager = new FileManager();
        $this->importer = new Importer();
	}

	public function handle(){
		try{
			$response = $this->handleRequest();
		}catch(Exception $e){
			$response = get_class($e).' in '.$e->getFile().' line '.$e->getLine().'. '.$e->getMessage();
            Logger::write($response, Logger::ERROR);
			if($e instanceof FailureException) {
				$response = "failure\n" . $response;
			}
		}
		return $response;
	}

	protected function handleRequest(){
		//Check if script already running
		if(!$this->locker->check()){
			throw new FailureException("Script already running");
		}
		//Lock script
		$this->locker->lock();
		//Handle HTTP auth
		if(!$this->auth->check()){
			throw new FailureException("Authorization error. Credentials mismatch");
		}
		//Get request params
		$type = $_GET['type'];
		$mode = $_GET['mode'];
        Logger::write('Request = type: '.$type.' mode: '.$mode, Logger::DEBUG);
		//Validate
		if($type != 'catalog'){
			throw new FailureException("Bad request parameter 'type'. Check protocol specification");
		}
		if(!in_array($mode, self::INVOKABLE_METHODS)){
			throw new FailureException("Bad request parameter 'mode'. Check protocol specification");
		}
		//Invoke
		$response = $this->$mode();
		//Unlock script
		$this->locker->release();
		return $response;
	}

	protected function checkAuth(){
		return "success\n".session_name()."\n".session_id();
	}

	protected function init(){
		$this->fileManager->cleanUploadPath();
		$this->importer->resetProgress();
		return "zip=".config('zip')."\nfile_limit=".config('file_limit');
	}

	protected function file(){
		$this->fileManager->saveFile($_GET['filename']);
		return "success";
	}

	protected function import(){
		$this->fileManager->extractZipIfExists();
        $filename = $_GET['filename'];
        Logger::write('filename import: '.$filename, Logger::DEBUG);
        if($filename == config('filename_import')){
            $type = 'catalogue';
        }elseif($filename == config('filename_offers')){
            $type = 'offers';
        }else{
            throw new FailureException("Unexpected 'filename' $filename");
        }
        $response = $this->importer->$type();
        if($response == 'success'){
            $this->registerAfter([$this->importer, $type.'After']);
        }
        return $response;
	}

    public function registerAfter(Callable $callback){
        $this->after = $callback;
    }

    public function after(){
        if(is_callable($this->after)){
            return call_user_func($this->after);
        }
    }
}