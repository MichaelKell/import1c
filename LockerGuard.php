<?php namespace Import1C;

class LockerGuard{

	protected $path;
	protected $secondsToAutorelease;

	public function __construct($path, $secondsToAutorelease = 600){
		$this->path = $path;
		$this->secondsToAutorelease = $secondsToAutorelease;
	}

	public function check(){
		list($time, $status) = file($this->path, FILE_IGNORE_NEW_LINES);
		if ($status != 'locked' || time() - $time > $this->secondsToAutorelease){
			return true;
		}
		return false;
	}

	public function lock(){
		$this->writeLocker('locked');
	}

	public function release(){
		$this->writeLocker('released');
	}

	protected function writeLocker($status){
		$fl = fopen($this->path, 'w');
		flock($fl, LOCK_EX);
		fwrite($fl, time()."\n".$status);
		fclose($fl);
	}
}