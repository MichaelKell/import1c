<?php namespace Import1C;

require_once '/vendor/autoload.php';

use Import1C\FrontController;
use App\Abante\Modules\Logger;

error_reporting(E_ERROR);
session_start();
set_time_limit(config('max_exec_time'));

$start_time = microtime(true);

Logger::setFile(config('log_path'));
if(config('log')){
    Logger::enable();
}else{
    Logger::disable();
}

Logger::write('---start--', Logger::DEBUG);
$controller = new FrontController();
$response = $controller->handle();

ob_start();
echo $response;
header("Connection: close");
header("Content-Length: ".ob_get_length());
ob_end_flush();
ob_flush();
flush();
$controller->after();