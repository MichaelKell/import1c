<?php
return [
	'max_exec_time' => 30,

	'filesize_limit' => 10*1024*1024,

	'zip' => 'yes',

	'upload_path' => '/runtime/1c/import/',

	'filename_import' => 'import0_1.xml',

	'filename_offers' => 'offers0_1.xml',

	'log' => true,

	'log_path' => '/runtime/1c/log.txt',

	'locker_path' => '/runtime/1c/lock.txt',

	'user' => 'admin',

	'password' => 'password'
];